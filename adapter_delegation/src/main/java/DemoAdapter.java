import adapter.HelpDeskItem;
import adapter.HelpDeskQueue;
import adapter.Queue2ListAdapter;

import java.util.ArrayList;

/**
 * Demo for the adapter pattern.
 * Adapter class Queue2ListAdapter transforms client calls to adaptee-klasse ArrayList
 * Strategy used: object-adapter  (delegation)
 */
public class DemoAdapter {
    public static void main(String[] args) {
        HelpDeskQueue myQueue = new Queue2ListAdapter();

        myQueue.enqueue(new HelpDeskItem("Can't log in"));
        myQueue.enqueue(new HelpDeskItem(5, "Server crash"));
        myQueue.enqueue(new HelpDeskItem("Mouse not working"));
        myQueue.enqueue(new HelpDeskItem(5, "Laptop on fire!"));

        myQueue.overviewNatural();
        myQueue.overviewByPriority();
    }
}

/*
OUTPUT:
Queue in natural order:
	2016-10-18T13:55:24.146:  1 Can't log in
	2016-10-18T13:55:24.146:  5 Server crash
	2016-10-18T13:55:24.146:  1 Mouse not working
	2016-10-18T13:55:24.146:  5 Laptop on fire!
Queue by priority:
	2016-10-18T13:55:24.146:  5 Server crash
	2016-10-18T13:55:24.146:  5 Laptop on fire!
	2016-10-18T13:55:24.146:  1 Can't log in
	2016-10-18T13:55:24.146:  1 Mouse not working
 */

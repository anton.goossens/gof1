import domain.Punt;
import domain.PuntSubscriber;

public class Demo {
    public static void main(String[] args) {
        Punt punt = new Punt(1, 2);
        PuntSubscriber subscriber = new PuntSubscriber();
        punt.subscribe(subscriber);
        punt.verdubbelX();
        punt.verdubbelY();
    }
}

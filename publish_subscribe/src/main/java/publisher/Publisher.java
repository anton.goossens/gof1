package publisher;

import java.util.ArrayList;
import java.util.List;

public interface Publisher {
     void subscribe(Subscriber subscriber);
    void unsubscribe(Subscriber subscriber);

}

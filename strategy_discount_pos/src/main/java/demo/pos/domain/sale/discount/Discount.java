package demo.pos.domain.sale.discount;

/**
 * @author Jan de Rijke.
 */
public interface Discount {
	double getDiscount(double amount);
}
